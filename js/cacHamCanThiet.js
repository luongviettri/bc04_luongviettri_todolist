import { Todo } from "./model.js";
// mảng toDo:
let toDoArr = [];
// lấy API về và render ra giao diện
////!: Lấy API về
const BASE_URL = "https://62eb6d51705264f263d8473c.mockapi.io/todoApp";
// !LOADING
const batLoading = () => {
    document.getElementById('loading').style.display = "flex";
}
const tatLoading = () => {
    document.getElementById('loading').style.display = "none";
}
// !!!!! CRUD
// !READ
const layToDo = () => {
    batLoading();
    axios({
        url: `${BASE_URL}`,
        method: "GET"
    }).then((res) => {
        tatLoading();
        let myData = res.data;
        ////!: render ra giao diện
        xuLyRender(myData);
    }).catch((err) => {
        tatLoading();
        console.log('err: ', err);
    })
}
layToDo();
// ! CREATE
export const themTodo = () => {
    // xử lý tên
    const ten = xuLyTen();
    // tạo thành đối tượng mới
    let newTodo = new Todo(ten, false);
    // đẩy lên API
    handleCreateAPI(newTodo);
    resetInput();
}
const handleCreateAPI = (newTodo) => {
    batLoading();
    axios({
        url: `${BASE_URL} `,
        method: "POST",
        data: newTodo
    }).then((res) => {
        layToDo();
    }).catch((err) => {
        console.log('err: ', err);
    });
}
const xuLyTen = () => {
    const newName = document.getElementById('newTask').value;
    return newName;
}
// ! DELETE
export const xoaTodo = (todoID) => {
    batLoading();
    axios({
        url: `${BASE_URL}/${todoID}`,
        method: "DELETE"
    }).then((res) => {
        layToDo();
    }).catch((err) => {
        console.log('err: ', err);
    });
}
// ! UPDATE
export const suaTodo = (todoID) => {
    // ! đẩy thông tin của object lên form
    dayThongTinLenForm(todoID);
    // ! thay đổi icon và chức năng của button thêm thành button sửa ===> gắn hàm update lên button sửa
    thayDoiButtonUpdate(todoID);

}
const thayDoiButtonUpdate = (todoID) => {
    // ! để button cũ là none
    setDisplayNone('#addItem');
    // ! gắn button sửa vào parentElement
    attackButton();
    // ! gắn hàm update cho button sửa
    let btnEdit = document.getElementById('editItem');
    btnEdit.addEventListener('click', () => {
        let editInput = document.getElementById('newTask').value;
        handleUpdate(todoID, editInput);
    })
}
const handleUpdate = (todoID, editInput) => {
    batLoading();
    // ! gọi API update
    axios({
        url: `${BASE_URL}/${todoID}`,
        method: "PUT",
        data: {
            name: editInput,
        }
    }).then((res) => {
        layToDo();
    }).catch((err) => {
        console.log(err);
    })
    // ! xóa d-none trong button thêm
    setDisplay('#addItem');
    //! xóa button sửa
    removeButton();
    //! reset lại input
    resetInput();
}
const resetInput = () => {
    document.getElementById('newTask').value = '';
}
const setDisplayNone = (selector) => {
    document.querySelector(`${selector}`).classList.add("d-none")
}
const setDisplay = (selector) => {
    document.querySelector(`${selector}`).classList.remove("d-none")
}
const attackButton = () => {
    let parentElement = document.querySelector('.card__add');
    let editButton = `
    <button id="editItem">
    <i class="fa fa-save"></i>
    </button>
    `
    parentElement.innerHTML += editButton;
}
const removeButton = () => {
    let parentElement = document.querySelector('.card__add');
    let child = parentElement.lastElementChild;
    parentElement.removeChild(child);

}
const dayThongTinLenForm = (todoID) => {
    batLoading();
    axios({
        url: `${BASE_URL}/${todoID}`,
        method: "GET"
    }).then((res) => {
        tatLoading();
        const todoObject = res.data;
        dayLen(todoObject);
    }).catch((err) => {
        tatLoading();
        console.log('err: ', err);
    })
}
const dayLen = (ObjectCanLay) => {
    document.getElementById('newTask').value = ObjectCanLay.name;
}
const xuLyRender = (toDoArr) => {
    let rows1 = ``;
    let rows2 = ``;
    let tasksEl1 = document.getElementById('completed');
    let tasksEl2 = document.getElementById('todo');

    toDoArr.forEach((todo) => {
        if (todo.completed) {

            let row =
                `
        <li class="d-flex" >
            <div class="flex-grow-1"> 
                <span>${todo.name}</span>
            </div>
            <div class="flex-grow-0">
                <span
                onclick = "suaTodo(${todo.id})"
                class="btn">
                <i class="fa fa-user-edit"></i>
                </span>
                <span onclick="xoaTodo(${todo.id})" class="btn">
                    <i class="fa fa-trash-alt "></i>
                </span>
                <span
                onclick= "checkTodo(${todo.id})"
                class="btn">
                    <i class="fa fa-check-double"></i>
                </span>
            </div>
            </li >
    `
            rows1 += row;

        } else {

            let row =
                `
    <li class="d-flex" >
        <div class="flex-grow-1"> 
            <span>${todo.name}</span>
        </div>
        <div class="flex-grow-0">
            <span
            onclick = "suaTodo(${todo.id})"
            class="btn">
            <i class="fa fa-user-edit"></i>
            </span>
            <span onclick="xoaTodo(${todo.id})" class="btn">
                <i class="fa fa-trash-alt "></i>
            </span>
            <span class="btn"  onclick= "checkTodo(${todo.id})">
                
                <i class="fa fa-check-double"></i>
            </span>


        </div>
        </li >
    `
            rows2 += row;

        }

    }

    )
    tasksEl1.innerHTML = rows1;

    tasksEl2.innerHTML = rows2;
}
// ! CHECK
export const checkTodo = (todoID) => {
    batLoading();
    axios({
        url: `${BASE_URL}/${todoID}`,
        method: "GET"
    }).then((res) => {
        const todoObject = res.data;
        axios({
            url: `${BASE_URL}/${todoID}`,
            method: "PUT",
            data: {
                completed: !todoObject.completed,
            }
        }).then((res) => {
            layToDo();
        }).catch((err) => {
            console.log(err);
        })
    }).catch((err) => {
        console.log('err: ', err);
    })
}
// ! SET NGÀY
export const caiDatNgay = () => {
    // ! DOM đến tag ngày
    let tagNgay = document.querySelector(".card__title p")
    // ! lấy dữ liệu ngày hôm nay và format
    let homNay = xuLyNgay();
    // ! set DOM
    tagNgay.innerHTML =
        `
    <h6 class="text-primary font-weight-bold">
    ${homNay}
    </h6>
    `
        ;
}
const xuLyNgay = () => {
    let today = new Date();
    let day = today.getDate();
    let month = today.getMonth() + 1;
    let year = today.getFullYear();
    let fullDate = `ngày ${day} tháng ${month} năm ${year} `;
    return fullDate;
}
// ! SORT
export const sapXepAZ = () => {
    // ! lấy mảng
    batLoading();
    axios({
        url: `${BASE_URL}`,
        method: "GET"
    }).then((res) => {
        tatLoading();
        let myData = res.data;
        myData.sort((a, b) => a.name.localeCompare(b.name))
        ////!: render ra giao diện
        xuLyRender(myData);
    }).catch((err) => {
        tatLoading();
        console.log('err: ', err);
    })
}
export const sapXepZA = () => {
    // ! lấy mảng
    batLoading();
    axios({
        url: `${BASE_URL}`,
        method: "GET"
    }).then((res) => {
        tatLoading();
        let myData = res.data;
        myData.sort((a, b) => b.name.localeCompare(a.name))
        ////!: render ra giao diện
        xuLyRender(myData);
    }).catch((err) => {
        tatLoading();
        console.log('err: ', err);
    })
}


